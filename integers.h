///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03c - Integers - EE 205 - Spr 2022
///
/// @file integers.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date   29_JAN_2022
///////////////////////////////////////////////////////////////////////////////

#define TABLE_HEADER1 "Datatype       bits bytes              Minimum              Maximum\n"
#define TABLE_HEADER2 "-------------- ---- ----- ------------------------- -------------------------\n"
#define TABLE_FORMAT_CHAR "%-14s %4d %5d %25d %25d\n"
#define TABLE_FORMAT_SHORT "%-14s %4ld %5ld %25d %25d\n"
#define TABLE_FORMAT_INT "%-14s %4ld %5ld %25d %25d\n"
#define TABLE_FORMAT_UINT "%-14s %4ld %5ld %25u %25u\n"
#define TABLE_FORMAT_LONG "%-14s %4ld %5ld %25ld %25ld\n"
#define TABLE_FORMAT_ULONG "%-14s %4lu %5lu %25lu %25lu\n"
